require("tk")

def parseDieExpression (expression)
	separatedExpression = expression.gsub("-d","-1d").gsub("-","+-").split("+").reject{|item| item.empty?}
	output = Array.new
	separatedExpression.each do |die|
		splitDie = die.split("d")
		outputItem = Hash.new
		if splitDie[0].empty?
			outputItem["count"] = 1
		else
			outputItem["count"] = splitDie[0].to_i
		end
		if splitDie[1].nil?
			outputItem["sides"] = 1
		else
			outputItem["sides"] = splitDie[1].to_i
		end
		output.push(outputItem)
	end
	return output
end

def roll (dice)
	expression = parseDieExpression(dice)
	output = 0
	expression.each do |die|
		result = 0
		if die["sides"] > 1
#			puts "Rolling " + die["count"].abs.to_s + " " + die["sides"].to_s + "-sided dice..."
			die["count"].abs.times do
				rolled = rand(1..die["sides"])
#				puts "  Rolled a " + rolled.to_s
				result += rolled
			end
		else
			result = die["count"].abs
#			puts "Factoring in constant term " + die["count"].to_s
		end
		if die["count"] > 0
#			puts "Adding sum " + result.to_s + " to " + output.to_s
			output += result
		else
#			puts "Subtracting sum " + result.to_s + " from " + output.to_s
			output -= result
		end
#		puts "Current total is " + output.to_s
	end
	return output.to_s
end

def main ()
	input = TkVariable.new
	output = TkVariable.new
	pack = { 'padx' => 15, 'pady' => 5 }
	
	root = TkRoot.new { title "Roller" }
	
	label = TkLabel.new(root,'text' => 'Enter die expression in form NdX+MdY+..+k')
	label.pack(pack)
	
	inputBox = TkEntry.new(root, 'textvariable' => input)
	inputBox.pack(pack)
	
	outputBox = TkEntry.new(root, 'state' => 'readonly', 'textvariable' => output)
	outputBox.pack(pack)
	
	rollButton = TkButton.new(root) do
		text 'Roll!'
		command proc { output.value = roll( input.value ) }
	end
	rollButton.pack(pack)
	
	Tk.mainloop
end

main()